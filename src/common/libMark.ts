import { GeneratorOptions } from "./generatorOptions.type";
import { generationMark } from "@vlr/razor/export";

export function libMark(options: GeneratorOptions): string {
  return generationMark(options.linefeed, "@vlr/tsgen-seed");
}
