import { ContentFile } from "@vlr/gulp-transform";

export function runGenerator(files: ContentFile[]): ContentFile[] {
  // tslint:disable-next-line:no-require-imports
  const { generateBuckets } = require("../../../build/src");
  return generateBuckets(files);
}
