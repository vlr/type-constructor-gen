import { testDataFolder, testDataSrcFolder } from "../../settings";
import { readFolders } from "./readFolders";
import { runGeneratorOnTestFolder } from "./runGeneratorOnTestFolder";
import { combineStreams } from "./combineStreams";

export function applyGenToTestData(): Promise<void> {
  const testDataSrc = testDataSrcFolder();
  const folders = readFolders(testDataSrcFolder());
  const streams = folders.map(f => runGeneratorOnTestFolder(testDataSrc, testDataFolder(), f));
  return combineStreams(streams);
}


