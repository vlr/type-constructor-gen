import * as fs from "fs";

export function readFolders(folder: string): string[] {
  return fs.readdirSync(folder, { withFileTypes: true })
    .filter(entry => entry.isDirectory())
    .map(entry => entry.name);
}
