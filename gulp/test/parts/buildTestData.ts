import { spawn } from "@vlr/spawn";

export function buildTestData(): Promise<void> {
  return spawn("compileTestFolder", "npx", "tsc", "--build", "tsconfig_testData.json");
}
