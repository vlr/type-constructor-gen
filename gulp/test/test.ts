import { series } from "gulp";
import { build } from "../build/build";
import { runTests, runTestCoverage } from "./parts";

export const test = series(build, runTests);

export const cover = series(build, runTestCoverage);
